#include "game.h"

#include <cmath>

#include "Config.h"
#include "Objects/Ball/Ball.h"
#include "Objects/Field/Field.h"
#include "Objects/Stick/Stick.h"
#include "Collision Tester/Collision Tester.h"

using namespace pool;
using namespace _ball;
using namespace _field;
using namespace _stick;
using namespace collisions;

namespace pool
{
	static bool checkBallsStillMoving()
	{
		bool stillMoving = whiteBall->getMoving() && !whiteBall->getHole();

		for (short i = 0; i < amountBalls; i++)
		{
			if (balls[i]->getMoving() && !balls[i]->getHole())
				stillMoving = true;
		}

		return stillMoving;
	}

	static Vector2 getBallSpeedDirection()
	{
		const float max_speed_magnitude = 150;
		const float speed_multiplier = .5;

		Vector2 whiteBallPos = whiteBall->getPosition();
		Vector2 mousePos = { static_cast<float>(GetMouseX()), static_cast<float>(GetMouseY()) };

		Vector2 ballSpeedDirection = { whiteBallPos.x - mousePos.x,  whiteBallPos.y - mousePos.y };

		ballSpeedDirection.x *= speed_multiplier;
		ballSpeedDirection.y *= speed_multiplier;

		float magnitude = sqrtf(ballSpeedDirection.x * ballSpeedDirection.x + ballSpeedDirection.y * ballSpeedDirection.y);

		if (magnitude > max_speed_magnitude)
		{
			return ballSpeedDirection = { ballSpeedDirection.x / magnitude * max_speed_magnitude, ballSpeedDirection.y / magnitude * max_speed_magnitude };
		}
		else
		{
			return ballSpeedDirection;
		}
	}

	//------------------------------------------------------------------------

	static void initialization()
	{
		InitWindow(screenWidth, screenHeight, "Pool game");
		SetTargetFPS(60);

		stick->init();

#pragma region BALL_INSTANTIATION
		short row = 1;
		short ballNumber = 0;

		float x, y;
		float ballGap = 2.0f;

		for (int i = 0; i < 6; i++)
		{
			x = -ballRadius * i + screenWidth / 2 - ballRadius;

			for (int j = 0; j < i; j++)
			{
				ballNumber++;
				x += ballRadius * 2;
				y = i * sqrt(3.f) * -ballRadius + screenHeight / 3;
				balls[ballNumber - 1] = new Ball(ballNumber, { x, y }, ballRadius, false);
			}
		}
#pragma endregion
	}

	static void input()
	{
		if (!checkBallsStillMoving() && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			whiteBall->setSpeed(getBallSpeedDirection());
		}
	}

	static void update()
	{
		whiteBall->update();

		for (short i = 0; i < amountBalls; i++)
		{
			balls[i]->update();
		}

#pragma region COLLISION_CHECK
		// Colision contra paredes
		setCollisionBallWall(whiteBall, field->getDimentions());

		for (short i = 0; i < amountBalls; i++)
		{
			setCollisionBallWall(balls[i], field->getDimentions());
		}

		// Colision bola blanca
		for (short i = 0; i < amountBalls; i++)
		{
			if (checkCollisionBallBall(balls[i], whiteBall))
			{
				collisionList.push_back({ balls[i], whiteBall });
			}
		}

		// Colision entre bolas
		for (short i = 0; i < amountBalls; i++)
		{
			for (short j = 0; j < amountBalls; j++)
			{
				if (checkCollisionBallBall(balls[j], balls[i]) && i != j)
				{
					collisionList.push_back({ balls[j], balls[i] });
				}
			}
		}
#pragma endregion

#pragma region APPLY_PHYSICS

		// Aplicacion de la fisica y overlap
		vector <pair<Ball*, Ball*>>::iterator it;
		for (it = collisionList.begin(); it < collisionList.end(); it++)
		{
			setCollisionBallBall(it->first, it->second);
			setCollisionBallBall(it->second, it->first);
		}

		// Reseteo lista colisiones
		collisionList.clear();
#pragma endregion

#pragma region IN_HOLE
		// Checkeo in hole
		collisions::checkCollisionBallPosition(whiteBall, field);

		for (short j = 0; j < amountBalls; j++)
		{
			collisions::checkCollisionBallPosition(balls[j], field);
		}

		// Reseteo bola blanca in hole
		if (whiteBall->getHole() && !checkBallsStillMoving())
		{
			whiteBall->setPosition({ screenWidth / 2,  screenHeight / 1.5f });
			whiteBall->setSpeed({ 0,0 });
			whiteBall->setHole(false);
		}
#pragma endregion
	}

	static void draw()
	{
		BeginDrawing();

		ClearBackground(BLACK);
		field->draw();

		for (int j = 0; j < amountBalls; j++)
		{
			balls[j]->draw();
		}

		whiteBall->draw();

		if (!checkBallsStillMoving())
		{
			DrawLineEx(whiteBall->getPosition(), { whiteBall->getPosition().x + getBallSpeedDirection().x, whiteBall->getPosition().y + getBallSpeedDirection().y }, 2.0f, RED);
			stick->draw(whiteBall->getPosition(), ballRadius);
		}

		EndDrawing();
	}

	void run()
	{
		initialization();

		while (!WindowShouldClose())
		{
			input();
			update();
			draw();
		}

		CloseWindow();
	}
}