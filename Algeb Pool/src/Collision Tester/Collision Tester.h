#ifndef COLLISION_CHECKS_AND_LOGIC_H
#define COLLISION_CHECKS_AND_LOGIC_H

#include "Objects/Ball/Ball.h"
#include "Objects/Field/Field.h"
#include "raylib.h"
#include <math.h>
#include <vector>

using namespace pool;
using namespace _ball;
using namespace _field;

using namespace std;

namespace pool
{
	namespace collisions
	{
		//Collision Vector
		extern vector <pair<Ball*, Ball*>> collisionList;

		//Collision testers and listers
		bool checkCollisionBallBall(Ball* ball1, Ball* ball2);
		void setCollisionBallBall(Ball* ball1, Ball* ball2);
		void setCollisionBallWall(Ball* ball, Rectangle field);
		void checkCollisionBallPosition(Ball* ball, Field* field);

		//Collision Solvers
		void moveBalls(Ball* ball1, Ball* ball2);

		//Utils
		float distance(Vector2 from, Vector2 to);
		float dot(Vector2 from, Vector2 to);
	}
}

#endif
